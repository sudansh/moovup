Salient features of the base app
- 
- Written in kotlin
- MVVM struture with data binding
- Divided the packages feature wise for easier future work on this app.
- Android Architecture components which help in implementing MVVM.
- LiveData and ViewModel for observing changes on data.
- Koin for dependency injection
- Room for local caching of data
- Tests using espresso, mockito and android architecture library.

Generate a maps key from https://developers.google.com/maps/documentation/android/start#get-key
and put it in google_maps_api.xml (debug/release builds)
