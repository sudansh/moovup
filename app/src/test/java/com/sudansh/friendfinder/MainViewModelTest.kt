package com.sudansh.friendfinder

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.Observer
import com.sudansh.friendfinder.data.Resource
import com.sudansh.friendfinder.repository.FriendRepository
import com.sudansh.friendfinder.repository.local.db.entity.Friend
import com.sudansh.friendfinder.ui.MainViewModel
import com.sudansh.friendfinder.util.mock
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations


@RunWith(JUnit4::class)
class MainViewModelTest {
    @get:Rule
    val rule = InstantTaskExecutorRule()

    private lateinit var viewModel: MainViewModel
    @Mock
    lateinit var repo: FriendRepository
    @Mock
    lateinit var results: Observer<Resource<List<Friend>>>

    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)

        viewModel = MainViewModel(repo)
        viewModel.friends.observeForever(results)
    }

    @Test
    fun testRefresh() {
        viewModel.friends.observeForever(mock())
        viewModel.refresh()
        //verify getFriends is called with true
        verify(repo).getFriends(true)
    }

    @Test
    fun testInitCall() {
        viewModel.friends.observeForever(mock())
        //verify getFriends is called with false
        verify(repo).getFriends(false)
    }

}