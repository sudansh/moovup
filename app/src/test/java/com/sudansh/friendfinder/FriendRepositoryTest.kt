package com.sudansh.friendfinder

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import com.sudansh.friendfinder.data.Resource
import com.sudansh.friendfinder.repository.FriendRepository
import com.sudansh.friendfinder.repository.local.db.dao.FriendDao
import com.sudansh.friendfinder.repository.local.db.entity.Friend
import com.sudansh.friendfinder.repository.remote.api.FriendApiService
import com.sudansh.friendfinder.util.InstantAppExecutors
import com.sudansh.friendfinder.util.mock
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito.*

@RunWith(JUnit4::class)
class FriendRepositoryTest {
    private val dao = mock(FriendDao::class.java)
    private val api = mock(FriendApiService::class.java)
    private val repo = FriendRepository(InstantAppExecutors(), dao, api)

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @Test
    fun loadUser() {
        repo.getFriends()
        verify(dao).findAll()
    }

    @Test
    fun testNetworkCall() {
        val dbData = MutableLiveData<List<Friend>>()
        `when`(dao.findAll()).thenReturn(dbData)
        val call = ApiUtil.successCall(createListFriends("foo", 123.0, 456.0, "bar", 10))
        `when`(api.getFriends()).thenReturn(call)
        val observer = mock<Observer<Resource<List<Friend>>>>()

        //fetch data from db
        repo.getFriends(false).observeForever(observer)

        //verify no apiService is called
        verify(api, never()).getFriends()

        val updatedDbData = MutableLiveData<List<Friend>>()
        `when`(dao.findAll()).thenReturn(updatedDbData)
        dbData.value = null

        //force fetch from apiService
        repo.getFriends(true).observeForever(observer)

        //verify apiService is called
        verify(api, times(1)).getFriends()
    }
}