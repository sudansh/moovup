package com.sudansh.friendfinder

import android.app.Application
import com.sudansh.friendfinder.di.appModule
import com.sudansh.friendfinder.di.localModule
import com.sudansh.friendfinder.di.remoteModule
import org.koin.android.ext.android.startKoin

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin(listOf(appModule, remoteModule, localModule))
    }

}