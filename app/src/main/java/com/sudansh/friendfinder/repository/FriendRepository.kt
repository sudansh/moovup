package com.sudansh.friendfinder.repository

import android.arch.lifecycle.LiveData
import com.sudansh.friendfinder.data.ApiResponse
import com.sudansh.friendfinder.data.AppExecutors
import com.sudansh.friendfinder.data.NetworkBoundResource
import com.sudansh.friendfinder.data.Resource
import com.sudansh.friendfinder.repository.local.db.dao.FriendDao
import com.sudansh.friendfinder.repository.local.db.entity.Friend
import com.sudansh.friendfinder.repository.remote.api.FriendApiService
import com.sudansh.friendfinder.testing.OpenForTesting

@OpenForTesting
class FriendRepository(
        val appExecutors: AppExecutors,
        val friendDao: FriendDao,
        val apiService: FriendApiService
) {

    fun getFriends(isFetch: Boolean = false): LiveData<Resource<List<Friend>>> {
        return object :
                NetworkBoundResource<List<Friend>, List<Friend>>(appExecutors) {
            override fun saveCallResult(item: List<Friend>) {
                friendDao.insert(item)
            }

            override fun shouldFetch(data: List<Friend>?): Boolean =
                    isFetch || data == null || data.isEmpty()

            override fun loadFromDb(): LiveData<List<Friend>> =
                    friendDao.findAll()

            override fun createCall(): LiveData<ApiResponse<List<Friend>>> =
                    apiService.getFriends()
        }.asLiveData()
    }
}