package com.sudansh.friendfinder.repository.remote.api

import android.arch.lifecycle.LiveData
import com.sudansh.friendfinder.data.ApiResponse
import com.sudansh.friendfinder.repository.local.db.entity.Friend
import retrofit2.http.GET


interface FriendApiService {

    @GET("api/json/get/cfdlYqzrfS")
    fun getFriends(): LiveData<ApiResponse<List<Friend>>>
}