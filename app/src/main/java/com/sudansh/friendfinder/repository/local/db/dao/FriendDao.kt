package com.sudansh.friendfinder.repository.local.db.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.sudansh.friendfinder.repository.local.db.entity.Friend

@Dao
interface FriendDao {

    @Query("SELECT * FROM friends")
    fun findAll(): LiveData<List<Friend>>

    @Query("SELECT * FROM friends WHERE id= :id")
    fun findById(id: String): LiveData<Friend>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(friend: Friend)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(list: List<Friend>)
}