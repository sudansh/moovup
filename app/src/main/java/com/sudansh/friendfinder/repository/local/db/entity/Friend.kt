package com.sudansh.friendfinder.repository.local.db.entity

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcelable
import com.google.android.gms.maps.model.LatLng
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "friends")
@Parcelize
data class Friend(
        @PrimaryKey @SerializedName("_id") val id: String,
        val name: String,
        val email: String,
        @Embedded val location: Location,
        @SerializedName("picture") val imageUrl: String
) : Parcelable {
    fun latlng() = LatLng(location.lat, location.lng)
}

@Parcelize
data class Location(
        @SerializedName("latitude") val lat: Double,
        @SerializedName("longitude") val lng: Double
) : Parcelable