package com.sudansh.friendfinder.repository.local.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.sudansh.friendfinder.repository.local.db.dao.FriendDao
import com.sudansh.friendfinder.repository.local.db.entity.Friend


@Database(entities = [Friend::class],
        exportSchema = false,
        version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun friendDao(): FriendDao
}