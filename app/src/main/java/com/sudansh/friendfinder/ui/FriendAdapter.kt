package com.sudansh.friendfinder.ui

import android.databinding.DataBindingUtil
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.sudansh.friendfinder.R
import com.sudansh.friendfinder.databinding.ItemFriendBinding
import com.sudansh.friendfinder.repository.local.db.entity.Friend


class FriendAdapter(private var callback: OnItemClickListener) : RecyclerView.Adapter<FriendViewHolder>() {
    private val listFriends: MutableList<Friend> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FriendViewHolder {
        val binding = DataBindingUtil.inflate<ItemFriendBinding>(
                LayoutInflater.from(parent.context),
                R.layout.item_friend,
                parent,
                false
        )
        return FriendViewHolder(binding)
    }

    override fun getItemCount() = listFriends.size

    override fun onBindViewHolder(vh: FriendViewHolder, position: Int) {
        val binding = vh.binding()
        binding.data = listFriends[position]
        binding.mainContainer.setOnClickListener {
            callback.onItemClick(listFriends[position], binding.image, binding.name)
        }
        binding.executePendingBindings()
    }

    fun updateItems(data: List<Friend>) {
        val diffResult = DiffUtil.calculateDiff(FriendDiffUtil(data, listFriends))
        listFriends.clear()
        listFriends.addAll(data)
        diffResult.dispatchUpdatesTo(this)
    }
}

class FriendViewHolder(private val binding: ItemFriendBinding) :
        RecyclerView.ViewHolder(binding.root) {

    fun binding() = binding
}

interface OnItemClickListener {
    fun onItemClick(friend: Friend, image: ImageView, name: TextView)
}
