package com.sudansh.friendfinder.ui

import android.support.v7.util.DiffUtil
import com.sudansh.friendfinder.repository.local.db.entity.Friend

class FriendDiffUtil(private val newList: List<Friend>, private val oldList: List<Friend>) : DiffUtil.Callback() {
    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
            newList[newItemPosition].id == oldList[oldItemPosition].id

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) =
            newList[newItemPosition] == oldList[oldItemPosition]

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size
}