package com.sudansh.friendfinder.ui

import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.MarkerOptions
import com.sudansh.friendfinder.R
import com.sudansh.friendfinder.databinding.ActivityMapsBinding
import com.sudansh.friendfinder.repository.local.db.entity.Friend

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private var friend: Friend? = null
    private lateinit var binding: ActivityMapsBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_maps)
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        friend = intent.getParcelableExtra(KEY_FRIEND)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        friend?.let {
            val markerLocation = it.latlng()
            mMap.addMarker(MarkerOptions().position(markerLocation))
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(markerLocation, 10.0f))
            binding.view!!.data = friend
        }
    }

    companion object {
        const val KEY_FRIEND = "KEY_FRIEND"

        fun start(context: Context, location: Friend) {
            Intent(context, MapsActivity::class.java).apply {
                putExtra(KEY_FRIEND, location)
            }.also { context.startActivity(it) }
        }
    }

}
