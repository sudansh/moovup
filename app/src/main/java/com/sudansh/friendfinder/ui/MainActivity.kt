package com.sudansh.friendfinder.ui

import android.app.ActivityOptions
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.util.Pair
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.sudansh.friendfinder.R
import com.sudansh.friendfinder.data.Resource
import com.sudansh.friendfinder.data.Status
import com.sudansh.friendfinder.databinding.ActivityMainBinding
import com.sudansh.friendfinder.repository.local.db.entity.Friend
import com.sudansh.friendfinder.util.action
import com.sudansh.friendfinder.util.observeNonNull
import com.sudansh.friendfinder.util.snack
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.architecture.ext.viewModel


class MainActivity : AppCompatActivity(), OnItemClickListener {
    private val viewModel by viewModel<MainViewModel>()
    private val friendAdapter by lazy { FriendAdapter(this) }
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.viewModel = viewModel
        binding.setLifecycleOwner(this)

        val divider = DividerItemDecoration(recyclerView.context, DividerItemDecoration.VERTICAL).apply {
            setDrawable(ContextCompat.getDrawable(baseContext, R.drawable.item_divider)!!)
        }

        with(recyclerView) {
            adapter = friendAdapter
            addItemDecoration(divider)
            itemAnimator = SlideInUpAnimator()
        }
        swipe.setOnRefreshListener(viewModel::refresh)

        viewModel.friends.observeNonNull(this) {
            updateUI(it)
        }
    }

    private fun updateUI(resource: Resource<List<Friend>>) {
        when (resource.status) {
            Status.SUCCESS -> {
                if (swipe.isRefreshing) swipe.isRefreshing = false
                friendAdapter.updateItems(resource.data.orEmpty())
                viewModel.isLoading.set(false)
            }
            Status.LOADING -> viewModel.isLoading.set(true)
            Status.ERROR -> {
                if (swipe.isRefreshing) swipe.isRefreshing = false
                viewModel.isLoading.set(false)
                showError(resource.message.orEmpty())
            }
        }

    }

    private fun showError(message: String) {
        mainContainer.snack(message, Snackbar.LENGTH_INDEFINITE) {
            action(getString(R.string.yes)) { viewModel.refresh() }
        }
    }

    override fun onItemClick(friend: Friend, image: ImageView, name: TextView) {
        val options = ActivityOptions.makeSceneTransitionAnimation(this,
                Pair.create<View, String>(image, getString(R.string.transitionImage)),
                Pair.create<View, String>(name, getString(R.string.transitionname)))
        Intent(this, MapsActivity::class.java).apply {
            putExtra(MapsActivity.KEY_FRIEND, friend)
        }.also { startActivity(it, options.toBundle()) }

    }
}