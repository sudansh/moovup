package com.sudansh.friendfinder.ui

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.databinding.ObservableBoolean
import com.sudansh.friendfinder.data.Resource
import com.sudansh.friendfinder.repository.FriendRepository
import com.sudansh.friendfinder.repository.local.db.entity.Friend
import com.sudansh.friendfinder.testing.OpenForTesting
import com.sudansh.friendfinder.util.switch

@OpenForTesting
class MainViewModel(private val repo: FriendRepository) : ViewModel() {
    private val refresh: MutableLiveData<Boolean> = MutableLiveData()
    val isLoading = ObservableBoolean(true)
    val friends: LiveData<Resource<List<Friend>>> =
            refresh.switch { startLoad ->
                repo.getFriends(startLoad)
            }

    init {
        refresh.value = false
    }

    fun refresh() {
        refresh.value = true
    }
}