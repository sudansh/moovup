package com.sudansh.friendfinder.di

import android.arch.persistence.room.Room
import com.sudansh.friendfinder.data.AppExecutors
import com.sudansh.friendfinder.repository.FriendRepository
import com.sudansh.friendfinder.repository.local.db.AppDatabase
import org.koin.dsl.module.applicationContext


val localModule = applicationContext {
    bean { AppExecutors() }
    bean { FriendRepository(get(), get(), get()) }
    bean {
        Room.databaseBuilder(get(), AppDatabase::class.java, "friend-db").build()
    }
    bean { get<AppDatabase>().friendDao() }
}