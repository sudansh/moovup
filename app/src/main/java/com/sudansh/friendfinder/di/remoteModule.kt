package com.sudansh.friendfinder.di

import com.sudansh.friendfinder.data.LiveDataCallAdapterFactory
import com.sudansh.friendfinder.repository.remote.api.FriendApiService
import org.koin.dsl.module.applicationContext
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val remoteModule = applicationContext {
    bean { createWebService<FriendApiService>() }
}

inline fun <reified T> createWebService(): T {
    return Retrofit.Builder()
            .baseUrl("http://www.json-generator.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(LiveDataCallAdapterFactory())
            .build().create(T::class.java)
}