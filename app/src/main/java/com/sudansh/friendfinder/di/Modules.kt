package com.sudansh.friendfinder.di

import com.sudansh.friendfinder.ui.MainViewModel
import org.koin.android.architecture.ext.viewModel
import org.koin.dsl.module.applicationContext


val appModule = applicationContext {

    viewModel { MainViewModel(get()) }
}