package com.sudansh.friendfinder

import com.sudansh.friendfinder.repository.local.db.entity.Friend
import com.sudansh.friendfinder.repository.local.db.entity.Location

fun createFriend(id: String, name: String, lat: Double, lng: Double, url: String): Friend {
    return Friend(id, name, "", Location(lat, lng), url)
}

fun createListFriends(name: String, lat: Double, lng: Double, url: String, count: Int = 10): List<Friend> {
    return (1 until count).map {
        Friend(it.toString(), name + it, "", Location(lat, lng), url + it)
    }
}