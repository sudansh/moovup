package com.sudansh.friendfinder.testing

@Target(AnnotationTarget.CLASS)
annotation class OpenForTesting
